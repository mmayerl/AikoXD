﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AikoXD {
    public partial class SelectUserForm : Form {
        public string User {
            get {
                return this.cbUser.SelectedValue.ToString();
            }
        }

        public SelectUserForm() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            //Get the list of availabe users from %AppData%\Skype
            
            var dirs = Directory.EnumerateDirectories(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Skype")).Select(_ => Path.GetFileName(_));
            var users = dirs.Where(_ => _ != "Content" && _ != "DataRv" && _ != "My Skype Received Files" && _ != "shared_dynco" && _ != "shared_httpfe").ToList();

            cbUser.DataSource = users;
        }

        private void btnOK_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void btnCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
