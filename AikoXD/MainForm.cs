﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.SQLite;

namespace AikoXD {
    public partial class MainForm : Form {
        private Timer retrieveTimer;
        private string conString;
        private long firstRetrieve;
        private long lastRetrieve;

        public MainForm() {   
            InitializeComponent();
            this.lastRetrieve = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            this.firstRetrieve = lastRetrieve;
        }

        private void MainForm_Load(object sender, EventArgs e) {
            var selectUser = new SelectUserForm();

            if (selectUser.ShowDialog() == DialogResult.OK) {
                this.conString = string.Format("Data Source={0};Version=3;", Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Skype", selectUser.User, "main.db"));
                
                this.retrieveTimer = new Timer();
                this.retrieveTimer.Interval = 10000;
                this.retrieveTimer.Tick += retrieveTimer_Tick;
                this.retrieveTimer.Start();
            }
            else {
                this.Close();
            }
        }

        void retrieveTimer_Tick(object sender, EventArgs e) {
            using (var con = new SQLiteConnection(this.conString)) {
                try {
                    con.Open();
                    UpdateLiveCounter(con);
                    UpdateStats(con);
                    con.Close();
                }
                catch (IOException) {
                    //Ignore IOExeptions - they are thrown when skype has locked to data base.
                }
                catch (SQLiteException) {
                    //See above
                }
            }
        }

        void UpdateLiveCounter(SQLiteConnection con) {
            using (var com = con.CreateCommand()) {
                //Get messages since last read
                com.CommandText = @"SELECT      datetime(timestamp, 'unixepoch') AS time, 
                                                timestamp,
                                                author, 
                                                body_xml 
                                    FROM        messages
                                    WHERE       timestamp > @cur
                                    ORDER BY    timestamp ASC";
                com.Parameters.AddWithValue("cur", this.lastRetrieve);

                using (var reader = com.ExecuteReader()) {
                    while (reader.Read()) {
                        var message = reader["body_xml"].ToString();
                        var noOfXDs = Regex.Matches(message, "[xX]+[dD]+").Count;

                        if (noOfXDs > 0) {
                            this.txtCounter.Text += string.Format("{0}: {1} just used {2} XDs.{3}", reader["time"], reader["author"], noOfXDs, Environment.NewLine);
                        }

                        this.lastRetrieve = (long)reader["timestamp"];
                    }
                }
            }
        }

        void UpdateStats(SQLiteConnection con) {
            using (var com = con.CreateCommand()) {
                //Get every message since program start
                com.CommandText = @"SELECT      author, 
                                                body_xml 
                                    FROM        messages
                                    WHERE       timestamp > @cur
                                    ORDER BY    timestamp ASC";
                com.Parameters.AddWithValue("cur", this.firstRetrieve);

                using (var reader = com.ExecuteReader()) {
                    var l = new List<XDPost>();

                    //Count number of XDs per user by creating a journal
                    while (reader.Read()) {
                        var message = reader["body_xml"].ToString();
                        var noOfXDs = Regex.Matches(message, "[xX]+[dD]+").Count;

                        if (noOfXDs > 0) {
                            l.Add(new XDPost(reader["author"].ToString(), noOfXDs));
                        }
                    }

                    //Group this journal to create final result
                    var counts = l.GroupBy(_ => _.User).Select(_ => new { User = _.Key, Count = _.Sum(x => x.XDCount) }).OrderByDescending(_ => _.Count).ToList();
                    this.dgvStats.DataSource = counts;

                    this.lblGrandTotal.Text = counts.Sum(_ => _.Count).ToString();
                }
            }
        }
    }
}
