﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AikoXD {
    class XDPost {
        public string User { get; set; }
        public int XDCount { get; set; }

        public XDPost(string user, int count) {
            this.User = user;
            this.XDCount = count;
        }
    }
}
